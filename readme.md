## Поднимаем окружение docker

> Убедитесь, что у Вас установлен docker & docker-compose `$ docker -v` и `$ docker-compose -v`

В корне проекта запускаем:

```
$ make start
```

Заходим в контейнер с приложением:

```
$ make app
```

> Производим дальнейшие действия, находясь внутри контейнера приложения!

## Запуск проекта
> Эти действия производим будучи внутри контейнера приложения `$ make app`

Разворачиваем зависимости:

```
$ composer install
```
```
$ npm install
```

Перегенерируем ключ приложения:

```
$ php artisan key:generate
```

Поскольку мы клонировали репозиторий, то необходимо выполнить команду по копированию файла окружения:

```
$ php -r "file_exists('.env') || copy('.env.example', '.env');"
```

Производим миграцию базы данных:

```
$ php artisan migrate
```

Сидируем тестовые данные:

```
$ php artisan db:seed
```

## Удаление проекта

Удаляем все таблицы и данные:

```
$ php artisan migrate:reset
```

Останавливаем окружение docker

```
$ make stop
```

> Удаляем папку с проектом