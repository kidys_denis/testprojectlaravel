Тестовое задание Laravel.

Описание:
Нужно сделать небольшое CRUD приложение на Laravel c использованием нескольких таблиц и связками между ними.

Вводные данные:
1) Таблицы 
   - Авторы/Authors (Имя/Name, Фамилия/Surname, Место проживания/PlaceOfLiving) +
   - Статьи/Articles (Название статьи/Title, Описание/Description, Автор/AuthorId (привязка к таблице Авторы)) +
   - Голоса/Voices (Автор/AuthorId (привязка к таблице Авторы), Статья/ArticleId (привязка к таблице Статьи), Голос/Voice (Like или Dislike)) +

Требования:
1) Создать миграции для всех таблиц сделать каскадные связи там где это требуется +
2) Написать модели под каждую таблицу +
3) Создать 2 страницы
   - Страница с таблицей всех статей (Поля: Название статьи, ФИО автора, Like, Dislike) сделать сортировку записей по разнице лайков и
     дизлайков (количество лайков - количество дизлайков) от большего к меньшему +
   - Страница c просмотром статьи +
4) Реализовать функционал создания статьи, удаления, изменения.

Реализацию выложить в репозиторий на bitbucket и разшарить доступ для просмотра для hbsup24@mail.com.

ЗАПРОСЫ:

1) Количество статей у каждого автора:
SELECT COUNT(DISTINCT `articles`.`title`) article, CONCAT(`authors`.`surname`, ' ', `authors`.`name`) author FROM `articles` INNER JOIN `authors` ON `authors`.`id`=`articles`.`authorId` GROUP BY `authors`.`id`

2) Голоса по статья в разрезе авторов:
SELECT `authors`.`id` AS authorId, `articles`.`id` AS articleId, `voices`.`voice` FROM `voices` INNER JOIN `authors` ON `authors`.`id`=`voices`.`authorId` INNER JOIN `articles` ON `articles`.`id`=`voices`.`articleId`

3) Достаем все like:
SELECT `articles`.`title` AS article, CONCAT(`authors`.`surname`, ' ', `authors`.`name`) author, `voices`.`voice` FROM (SELECT `voices`.`voice`, `voices`.`articleId` FROM `voices` WHERE `voices`.`voice`='like') voices INNER JOIN `articles` ON `articles`.`id`=`voices`.`articleId` INNER JOIN `authors` ON `authors`.`id`=`articles`.`authorId`

4) Формируем список статей и лайков/дизлайков по ним:
SELECT CONCAT(`authors`.`surname`, ' ', `authors`.`name`) author, likes.article, likes.likes, dislikes.dislikes FROM (SELECT `articles`.`authorId`, `articles`.`title` AS article, COUNT(`voices`.`voice`) likes FROM (SELECT `voices`.`voice`, `voices`.`articleId` FROM `voices` WHERE `voices`.`voice`='like') voices INNER JOIN `articles` ON `articles`.`id`=`voices`.`articleId` GROUP BY `voices`.`voice`, `articles`.`title`, `articles`.`authorId`) likes INNER JOIN (SELECT `articles`.`authorId`, `articles`.`title` AS article, COUNT(`voices`.`voice`) dislikes FROM (SELECT `voices`.`voice`, `voices`.`articleId` FROM `voices` WHERE `voices`.`voice`='dislike') voices INNER JOIN `articles` ON `articles`.`id`=`voices`.`articleId` GROUP BY `voices`.`voice`, `articles`.`title`, `articles`.`authorId`) dislikes ON likes.article=dislikes.article INNER JOIN `authors` ON `authors`.`id`=dislikes.authorId