<?php

use Illuminate\Database\Seeder;
use App\Author;
use App\Article;
use App\Voice;

/**
 * Class AuthorsTableSeeder
 */
class AuthorsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $authorIds = factory(Author::class, 5)->create()->pluck('id')->toArray();

        $articles = factory(Article::class, rand(10, 20))->make()->each(function ($article) use($authorIds) {
            $article->authorId = rand(1, count($authorIds));
        })->toArray();
        Article::insert($articles);

        $voices = factory(Voice::class, rand(20, 50))->make()->each(function ($voice) use($authorIds, $articles) {
            $voice->authorId = rand(1, count($authorIds));
            $voice->articleId = rand(1, count($articles));
        })->toArray();

        Voice::insert($voices);
    }
}
