<?php

/** @var Factory $factory */

use App\Author;
use Faker\Generator as Faker;
use Illuminate\Database\Eloquent\Factory;

$factory->define(Author::class, function (Faker $faker) {
    $timestamps = $faker->dateTimeBetween('-14 days','-1 days');

    return [
        'name' => $faker->name(rand(32, 64)),
        'surname' => $faker->name(rand(32, 64)),
        'placeOfLiving' => $faker->city,
        'created_at' => $timestamps,
        'updated_at' => $timestamps
    ];
});
