<?php

/** @var Factory $factory */

use App\Article;
use Faker\Generator as Faker;
use Illuminate\Database\Eloquent\Factory;

$factory->define(Article::class, function (Faker $faker) {
    $timestamps = $faker->dateTimeBetween('-14 days','-1 days');

    return [
        'title' => $faker->text(rand(64, 128)),
        'description' => $faker->text(rand(512, 1024)),
        'created_at' => $timestamps,
        'updated_at' => $timestamps
    ];
});
