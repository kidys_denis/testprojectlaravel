<?php

/** @var Factory $factory */

use App\Voice;
use Faker\Generator as Faker;
use Illuminate\Database\Eloquent\Factory;

$factory->define(Voice::class, function (Faker $faker) {
    $timestamps = $faker->dateTimeBetween('-14 days','-1 days');

    return [
        'voice' => $faker->randomElement(['like', 'dislike']),
        'created_at' => $timestamps,
        'updated_at' => $timestamps
    ];
});
