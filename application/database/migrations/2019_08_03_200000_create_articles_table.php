<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Class CreateArticlesTable
 */
class CreateArticlesTable extends Migration
{
    /**
     * @var string
     */
    private $_tableName = 'articles';

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->_tableName, function (Blueprint $table) {
            $table->bigIncrements('id')->unsigned();
            $table->string('title', 128);
            $table->text('description');
            $table->unsignedBigInteger('authorId');
            $table->timestamps();

            $table->foreign('authorId')->references('id')->on('authors')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table($this->_tableName, function (Blueprint $table) {
            $table->dropForeign($this->_tableName . '_authorId_foreign');
        });
        Schema::dropIfExists($this->_tableName);
    }
}
