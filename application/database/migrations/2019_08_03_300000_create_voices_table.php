<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Class CreateVoicesTable
 */
class CreateVoicesTable extends Migration
{
    /**
     * @var string
     */
    private $_tableName = 'voices';

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->_tableName, function (Blueprint $table) {
            $table->bigIncrements('id')->unsigned();
            $table->unsignedBigInteger('authorId');
            $table->unsignedBigInteger('articleId');
            $table->enum('voice', ['like', 'dislike']);
            $table->timestamps();

            $table->foreign('authorId')->references('id')->on('authors')->onDelete('cascade');
            $table->foreign('articleId')->references('id')->on('articles')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table($this->_tableName, function (Blueprint $table) {
            $table->dropForeign($this->_tableName . '_authorId_foreign');
            $table->dropForeign($this->_tableName . '_articleId_foreign');
        });
        Schema::dropIfExists($this->_tableName);
    }
}
