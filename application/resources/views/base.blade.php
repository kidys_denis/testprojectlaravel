<!DOCTYPE html>
<html lang="en">
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="csrf-token" content="{{csrf_token()}}">
    <title>Test Laravel Project!</title>
    <link href="{{asset('css/app.css')}}" rel="stylesheet" type="text/css" />
</head>
<body>
<br/>
<div class="container">
    <div class="row justify-content-center">
        <div class="col-auto">
            <a href="{{route('articles.create')}}" class="btn btn-success">Create new article?!</a>
        </div>
        <div class="col-auto">
            <a href="{{route('index')}}" class="btn btn-primary">Go to index page</a>
        </div>
    </div>
    <hr class="w-100"/>
    <div class="row">
        <div class="col-12">
            <h1>{{$title ?? 'List articles'}}</h1>
        </div>
    </div>
    <hr class="w-100"/>
    @yield('main')
</div>
<script src="{{asset('js/app.js')}}" type="text/javascript"></script>
</body>
</html>
