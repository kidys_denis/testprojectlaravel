@extends('base')

@section('main')
<div class="row">
    <div class="col-12">
        <div class="article-item">
            <div class="article-nav row justify-content-end">
                <div class="col-auto">
                    <div class="row no-gutters">
                        <a class="btn btn-primary" href="{{route('articles.edit', ['id' => $article['article']['id']])}}">Edit</a>
                        &nbsp;
                        <form action="{{route('articles.destroy', ['id' => $article['article']['id']])}}" method="post" class="col-auto">
                            @csrf
                            @method('DELETE')
                            <button type="submit" class="btn btn-danger">Delete</button>
                        </form>
                    </div>
                </div>
            </div>
            <hr class="w-100"/>
            <div class="card card_kidys_theme">
                <div class="card-header">
                    <h3>Title is:&nbsp;<small>{{$article['article']['title']}}</small></h3>
                </div>
                <div class="card-body">
                    {{$article['article']['description']}}
                </div>
                <div class="card-footer">
                    <div class="row justify-content-between">
                        <div class="col-6"><strong>Author is</strong>:&nbsp;{{$article['author']['surname']}} {{$article['author']['name']}}</div>
                        <div class="col-6 text-right"><strong>Likes is</strong>:&nbsp;{{$article['voices']['likes']}} | <strong>Dislikes is</strong>:&nbsp;{{$article['voices']['dislikes']}}</div>
                    </div>
                </div>
            </div>
        </div>
        <hr class="w-100"/>
    </div>
</div>
@endsection