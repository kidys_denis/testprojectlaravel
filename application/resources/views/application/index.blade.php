@extends('base')

@section('empty-articles')
<div class="alert alert-danger">
    Articles empty...
</div>
@endsection

@section('main')
<div class="articles-list">
    @isset($articles)
        @if(count($articles) > 0)
            @foreach($articles as $article)
            <div class="card card_kidys_theme">
                <div class="card-header">
                    <h3>Title is:&nbsp;<small><a href="{{route('article_show', ['id' => $article['article']['id']])}}">{{$article['article']['title']}}</a></small></h3></div>
                <div class="card-body">
                    {{$article['article']['description']}}
                </div>
                <div class="card-footer">
                    <div class="row justify-content-between">
                        <div class="col-6"><strong>Author is</strong>:&nbsp;{{$article['author']['surname']}} {{$article['author']['name']}}</div>
                        <div class="col-6 text-right"><strong>Likes is</strong>:&nbsp;{{$article['likes']}} | <strong>Dislikes is</strong>:&nbsp;{{$article['dislikes']}}</div>
                    </div>
                </div>
            </div>
            @endforeach
            <hr class="w-100"/>
            <div class="pagination">{{$articlesPagination->links()}}</div>
        @endif
    @endisset
    @empty($articles)
        @yield('empty-articles')
    @endempty
</div>
<hr class="w-100"/>
@endsection