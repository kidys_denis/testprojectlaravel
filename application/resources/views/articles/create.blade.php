@extends('base')

@section('main')
<div class="row">
    <div class="col-12">
        @if ($errors->any())
            <div class="alert alert-danger alert-errors-form">
                <ul class="form-errors">
                    @foreach ($errors->all() as $error)
                        <li>{{$error}}</li>
                    @endforeach
                </ul>
            </div>
        @endif
    </div>
    <div class="col-12">
        <form action="{{route('articles.store')}}" method="post">
            @csrf
            <input type="hidden" name="authorId" value="1"/>
            <div class="form-group">
                <label for="title">Title:</label>
                <input id="title" type="text" class="form-control" name="title"/>
            </div>
            <div class="form-group">
                <label for="description">Description:</label>
                <textarea id="description" class="form-control" rows="5" name="description"></textarea>
            </div>
            <hr class="w-100"/>
            <div class="text-right">
                <button type="submit" class="btn btn-primary">Create article</button>
            </div>
        </form>
    </div>
</div>
<hr class="w-100"/>
@endsection