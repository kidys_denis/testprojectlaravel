<?php

namespace App;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * @property Author $author
 * @property Voice $voices
 * @method static Model|Collection|null find(mixed $key, mixed $default = null)
 */
class Article extends Model
{
    /**
     * To the author of the article
     * @return BelongsTo
     */
    public function author()
    {
        return $this->belongsTo(Author::class, 'authorId');
    }

    /**
     * Get article voices
     * @return HasMany
     */
    public function voices()
    {
        return $this->hasMany(Voice::class, 'articleId');
    }

    /**
     * @return array
     */
    public static function getAllSortByVoicesDefference()
    {
        $articles = Article::paginate(3);
        $articlesWithVoices = [];

        foreach ($articles as $key => $article) {
            $voicesCurrentArticle = $article->voices;
            $articlesWithVoices[$key]['author'] = $article->author->toArray();
            $articlesWithVoices[$key]['article'] = $article->toArray();
            $articlesWithVoices[$key]['difference'] = 0;
            $articlesWithVoices[$key]['likes'] = 0;
            $articlesWithVoices[$key]['dislikes'] = 0;

            foreach ($voicesCurrentArticle as $voice) {
                if ($voice['voice'] === Voice::DISLIKE) {
                    $articlesWithVoices[$key]['difference'] = --$articlesWithVoices[$key]['difference'];
                    $articlesWithVoices[$key]['dislikes'] = ++$articlesWithVoices[$key]['dislikes'];
                } else {
                    $articlesWithVoices[$key]['difference'] = ++$articlesWithVoices[$key]['difference'];
                    $articlesWithVoices[$key]['likes'] = ++$articlesWithVoices[$key]['likes'];
                }
            }
        }

        usort($articlesWithVoices, function($a, $b) {
            if ($a['difference'] == $b['difference']) {
                return 0;
            }
            return $a['difference'] > $b['difference'] ? -1 : 1;
        });

        return [
            $articlesWithVoices,
            $articles
        ];
    }

    /**
     * @param int $id
     * @return array
     */
    public static function getArticleWithVoices($id)
    {
        $collectionArticle = Article::findOrFail((int)$id);
        $voices = $collectionArticle->voices->toArray();

        $article = [];
        $article['article'] = $collectionArticle->toArray();
        $article['author'] = Author::find((int)$article['article']['authorId'])->toArray();
        $article['voices'] = [
            'likes' => 0,
            'dislikes' => 0
        ];

        foreach ($voices as $voice) {
            if ($voice['voice'] === Voice::DISLIKE) {
                $article['voices']['dislikes'] = ++$article['voices']['dislikes'];
            } else {
                $article['voices']['likes'] = ++$article['voices']['likes'];
            }
        }

        return $article;
    }
}
