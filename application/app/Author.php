<?php

namespace App;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * @property Article $articles
 * @method static Model|Collection|null find(mixed $key, mixed $default = null)
 */
class Author extends Model
{
    /**
     * Get articles by author
     * @return HasMany
     */
    public function articles()
    {
        return $this->hasMany(Article::class, 'authorId');
    }
}
