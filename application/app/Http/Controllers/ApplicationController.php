<?php

namespace App\Http\Controllers;

use App\Article;
use Illuminate\Http\Request;

class ApplicationController extends Controller
{
    public function index()
    {
        $groupArticles = Article::getAllSortByVoicesDefference();
        $articles = $groupArticles[0];
        $articlesPagination = $groupArticles[1];

        return view('application.index', compact('articles', 'articlesPagination'));
    }

    public function show(Request $request)
    {
        $article = Article::getArticleWithVoices((int)$request->route('id'));
        $title = 'Article info';

        return view('application.show', compact('article', 'title'));
    }
}
