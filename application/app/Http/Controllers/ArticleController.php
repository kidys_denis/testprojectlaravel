<?php

namespace App\Http\Controllers;

use App\Article;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class ArticleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        return redirect('/', 302);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $title = 'Create new article';
        $type_query = 'store';

        return view('articles.create', compact('title', 'type_query'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'title' => [
                'required',
                'min:64',
                'max:128'
            ],
            'description' => 'required'
        ]);

        $article = new Article();
        $article->title = $request->title;
        $article->description = $request->description;
        $article->authorId = (int)$request->authorId ?? 1;
        $article->save();

        return redirect()->route('index');
    }

    /**
     * Display the specified resource.
     *
     * @param Request $request
     * @return Response
     */
    public function show(Request $request)
    {
        $id = (int)$request->route('id');

        return redirect()->route('article_show', ['id' => $id]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        $title = 'Edit article';
        $article = Article::getArticleWithVoices((int)$id);

        return view('articles.edit', compact('title', 'article'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'title' => [
                'required',
                'min:64',
                'max:128'
            ],
            'description' => 'required'
        ]);

        $article = Article::findOrFail((int)$id);
        $article->title = $request->title;
        $article->description = $request->description;
        $article->authorId = (int)$request->authorId ?? 1;
        $article->save();

        return redirect()->route('article_show', ['id' => (int)$id]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return Response
     * @throws Exception
     */
    public function destroy($id)
    {
        $news = Article::find((int)$id);
        $news->delete();

        return redirect()->route('index');
    }
}
