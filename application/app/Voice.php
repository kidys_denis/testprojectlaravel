<?php

namespace App;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * @property Author $author
 * @property Article $article
 * @method static Model|Collection|null find(mixed $key, mixed $default = null)
 */
class Voice extends Model
{
    const DISLIKE = 'dislike';
    const LIKE = 'like';

    /**
     * To the author
     * @return BelongsTo
     */
    public function author()
    {
        return $this->belongsTo(Author::class, 'authorId');
    }

    /**
     * To the article
     * @return BelongsTo
     */
    public function article()
    {
        return $this->belongsTo(Article::class, 'articleId');
    }
}
