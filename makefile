start:
	@docker-compose up -d

stop:
	@docker-compose down --remove-orphans

app:
	@docker exec -it kidys--php-7.3-fpm bash

server-show:
	@docker ps

laravel-init:
	@sudo chown -R kidys:kidys ./application
	@sudo chmod -R 755 ./application
	@sudo chmod -R 777 ./application/storage

db-init:
	@sudo chown -R kidys:kidys ./db
	@sudo chmod -R 777 ./db